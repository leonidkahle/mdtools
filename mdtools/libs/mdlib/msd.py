# -*- coding: utf-8 -*-
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"
def get_mean_msd_of_block(args):
    block_start, range_for_t, zipped_trajectories, timestep_in_fs, index_start_vaf_is_0, block_length = args
    msd_isotrop_this_block = [
        np.mean(
            [
                np.linalg.norm(trajectory_of_ion[tau + t] - trajectory_of_ion[tau])**2 
                for tau in np.arange(block_start, block_start + block_length)
                for trajectory_of_ion in zipped_trajectories
            ]
        )
        for t in range_for_t
    ]

    slope_this_block, intercept_this_block, r_value, p_value, std_err = linregress(
        timestep_in_fs * range_for_t[index_start_vaf_is_0:],
        msd_isotrop_this_block[index_start_vaf_is_0:]
    )
    return slope_this_block, intercept_this_block, msd_isotrop_this_block

