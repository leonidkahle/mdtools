import sys
from ase import Atoms
from ase.io import read 
from ase.data import atomic_numbers
import json


"""
TODO:
*   rewrite structure dict as in Aiida and
*   give vacuum and cell boundaries to structures imprted from XYZ
"""
#~ from carlo.codes.kohn.pw.pwimmigrant import get_input_structure


def read_file_using_ase(filepath):
    """
    Let's use ASE instead of coding everything myself. 
    Input is a valid filepath, output an ase.Atoms instance.
    A list of known formats::

        =========================  ===========
        format                     short name
        =========================  ===========
        GPAW restart-file          gpw
        Dacapo netCDF output file  dacapo
        Old ASE netCDF trajectory  nc
        Virtual Nano Lab file      vnl
        ASE pickle trajectory      traj
        ASE bundle trajectory      bundle
        GPAW text output           gpaw-text
        CUBE file                  cube
        XCrySDen Structure File    xsf
        Dacapo text output         dacapo-text
        XYZ-file                   xyz
        VASP POSCAR/CONTCAR file   vasp
        VASP OUTCAR file           vasp_out
        SIESTA STRUCT file         struct_out
        ABINIT input file          abinit
        V_Sim ascii file           v_sim
        Protein Data Bank          pdb
        CIF-file                   cif
        FHI-aims geometry file     aims
        FHI-aims output file       aims_out
        VTK XML Image Data         vti
        VTK XML Structured Grid    vts
        VTK XML Unstructured Grid  vtu
        TURBOMOLE coord file       tmol
        TURBOMOLE gradient file    tmol-gradient
        exciting input             exi
        AtomEye configuration      cfg
        WIEN2k structure file      struct
        DftbPlus input file        dftb
        CASTEP geom file           cell
        CASTEP output file         castep
        CASTEP trajectory file     geom
        ETSF format                etsf.nc
        DFTBPlus GEN format        gen
        CMR db/cmr-file            db
        CMR db/cmr-file            cmr
        LAMMPS dump file           lammps
    """
    atoms = read(filepath)
    return atoms 


def structure_wizard(structure, need_ase=False,
        need_dict=False, need_aiida=False):
    """
    The structure wizard converts a structure to the format needed.

    :param structure: Some abstraction of a structure
    :param needase: A boolean value, and if ``True`` 
        this function will return the ase-instance
    :param needdict: (boolean), if ``True`` returns a dictionary with more information.
    
    Example of a returned dictionary::
        
            structure_dict = {
                'positions':atoms.get_positions().tolist(),
                'scaled_positions': atoms.get_scaled_positions().tolist(),
                'cell': atoms.get_cell().tolist(),
                'atomic_numbers': atoms.get_atomic_numbers().tolist(),
                'atoms': [ atom.symbol  for atom in atoms] ,
                'volume':atoms.get_volume(),
                'composition': {
                        i:structure_dict['atoms'].count(i) 
                        for i in set(structured['atoms'])
                    }
            }
            
    If not **needdict** and not **needase**, returns the following dictionary::

        {'content': structure_dict, 'name':name}
        
    The Structure can be:

    *   A dictionary or a json-instance, which has the keys
        `positions`, `atoms` and `cell`. 
        For the case of the H2-molecule this could be::
    
            structure = {
                'atoms': ['H', 'H'],
                'positions': [
                    [0,0,0], [0.5,0.5,0.5]
                ],
                'cell' = [
                    [2,0,0],
                    [0,2,0],
                    [0,0,2],
                ]
            }
        
        Unit assumed is always Angstrom.
    
    *   A pw-input file, which is read using 
        :func:`~carlo.codes.kohn.pw.pwparser.get_input_structure`
    *   Something that ASE can read using ase.io.read.
        Uses a wrapper for ASE in :func:`read_file_using_ase`.
    
    The name is chosen from the composition and used as label in the database.
    
    """
    
    returndict = {}
    
    if type(structure) is str:
        # Assuming this is a filepath
        f = open(structure)
        txt = f.read()
        returndict['origin'] = {'text':txt, 'filename':structure}
        if structure.endswith('.json'):
            structure = json.load(f)
        elif structure.endswith('.in'):
            structure = get_input_structure(txt)
        else:
            structure =  read_file_using_ase(structure)
        f.close()

    if type(structure) is dict:
        if set(structure.keys()).difference(['atoms', 'positions']):
            raise NotImplemented
            aiidas = AiidaStruc()
            [
                aiidas._set_attr(key, val)
                for key, val
                in structure.items()
                if key in ('sites', 'pbc', 'kinds', 'cell')
            ]
            atoms = aiidas.get_ase()
        else:
            atoms = Atoms(
                    [str(i) for i in structure['atoms']],
                    structure['positions'],
                    cell=structure['cell']
                )
    elif  isinstance(structure, Atoms):
        atoms = structure
    else:
        raise Exception("{} is not a valid input".format(structure))
    # Ok, everything is atoms now
    returndict['ase'] = atoms
    
    symbols = atoms.get_chemical_symbols()
    returndict['chemical_symbols'] = symbols
    _symbol_counts = [(symbol,symbols.count(symbol)) for symbol in set(symbols)]
    composition = {symbol:count for symbol, count in _symbol_counts}
    formula = ''.join(['{}{}'.format(symbol, count)  for symbol, count in _symbol_counts])

    returndict['formula'] = formula
    
    try:
        from aiida.orm.data.structure import StructureData as AiidaStruc
        struc = AiidaStruc()
        struc.set_ase(atoms)
        #~ struc._set_attr('pbc1', True)
        #~ struc._set_attr('pbc2', True)
        #~ struc._set_attr('pbc3', True)
        struc._set_attr('composition', composition)
        struc.label = formula
        returndict['structure'] = struc
    except Exception:
        pass

    if False:
        raise DeprecationWarning()
        structured = {}
        structured['positions'] = atoms.get_positions().tolist()
        structured['scaled_positions'] = atoms.get_scaled_positions().tolist()
        structured['cell'] = atoms.get_cell().tolist()
        structured['atomic_numbers'] = atoms.get_atomic_numbers().tolist()
        structured['atoms'] = [atom.symbol  for atom in atoms] 
        structured['volume'] = atoms.get_volume()
        structured['composition'] = {i:structured['atoms'].count(i) for i in set(structured['atoms'])}
        
        if need_dict:
            returnval = structured
        else:
            name = '-'.join(['{}_{}'.format(key, val) for key, val in  structured['composition'].items()])
            returnval = [{'content': structured, 'name':name}]

    return returndict


def import_from_pymatgen( *args, **kwargs):
    """
    Wrapper for pymatgen, builds a connection to materialsproject database <materialsproject.org>
    """
    import pymatgen as mg
    import cPickle
    from pymatgen.matproj.rest import MPRester
    from pymatgen.core.composition import Composition
    from pymatgen.core.periodic_table import symbol_from_Z as sym
    
    log = kwargs.get('log', sys.stdout)
    
    API_KEY = "k0Es5479n2TrLe1R"
    #make something to make the input work  
    
    earth_alkalines = ['Mg', 'Ca' ] #, 'Sr', 'Ba']
    tetravalent = ['Ti'] #, 'Zr', 'Ce', 'Nb','Sn']
    trivalent = ['Al', 'Sc','Y']
    chalcogenides = ['O', 'S']
    min_gap = 1.5
    def get_sum(compound, group):
        return sum([Composition(compound["formula"])[element] for element in group])
    
    #~ parsed_args = ps.parse_args(sys.argv[1:])
    periodic_table = [sym(i) for i in range(1,104)]
    elements_wanted = earth_alkalines + tetravalent +trivalent + chalcogenides
    elements_unwanted = list(set(periodic_table).difference(set(elements_wanted)))
    
    accepted_by_ase_calc = ['Al','Cu','Ag','Au', 'Ni','Pd', 'Pt', 'H','C','N', 'O']
    not_accepted_by_ase_calc = list(set(periodic_table).difference(set(accepted_by_ase_calc)))

    query1 = {"$and":[\
                                {'elements':{"$in":earth_alkalines}},
                                {'elements':{"$in":tetravalent}},
                                {'elements':{"$in":chalcogenides}},
                                {'elements':{"$nin":elements_unwanted}},
                                {'band_gap':{"$gt":min_gap}} ]}
    query2 =  {"$and":[\
                    {'elements':{'$in':accepted_by_ase_calc}},
                    {'elements':{"$nin":not_accepted_by_ase_calc}},
                    ]
                }
    
    with MPRester(api_key = API_KEY) as m:
        mlist = m.query(query2,
                                ["formula", "band_gap", "material_id", "spacegroup", "volume"])
                                
    log.write('\n\n\n')
    log.write(' '*4 + "|----------|----------|----------|----------|-------------|\n")
    log.write(' '*4 + "| mat-ID   | Formula  | band gap | point gr.| volume      |\n")
    log.write(' '*4 + "|----------|----------|----------|----------|-------------|\n")
    
    return_list = []
    struc_list = []
    print len(mlist)
    struc_set = set()
    
    for compound in mlist:
        if not(get_sum(compound, chalcogenides) == 3*get_sum(compound, earth_alkalines)): continue
        c = ''.join([str(key)+str(int(val)) for key, val in Composition(compound["formula"]).items() if str(key) in earth_alkalines])\
            + ''.join([str(key)+str(int(val)) for key, val in Composition(compound["formula"]).items() if str(key) in trivalent])\
            +''.join([str(key)+str(int(val)) for key, val in Composition(compound["formula"]).items() if str(key) in tetravalent])\
            +''.join([str(key)+str(int(val)) for key, val in Composition(compound["formula"]).items() if str(key) in chalcogenides])
        if not c:
            c = ''.join([str(key)+str(int(val)) for key, val in Composition(compound["formula"]).items()])
        unic = u"{}".format(c.replace('1', u'\u2081').replace('2',u'\u2082').\
                replace('3',u'\u2083').replace('4',u'\u2084').\
                replace('5',u'\u2085').replace('6',u'\u2086').\
                replace('7',u'\u2087').replace('8',u'\u2088').\
                replace('9',u'\u2089').replace('0',u'\u2080'))
                
        log.write(u"    | {:<9}| {:<9}| {:<9}| {:<9}| {:<9} |\n".format(compound["material_id"].replace('mp-',''), 
                unic, compound["band_gap"], compound["spacegroup"]['point_group'], compound["volume"]))
        #~ log.write(u"    | {:<9}| {:<9}| {:<9}| {:<9}| {:<9} |\n".format(compound["material_id"].replace('mp-',''), 
                #~ compound["formula"], compound["band_gap"], compound["spacegroup"]['point_group'], compound["volume"]))
        
        entry = m.get_entry_by_material_id(compound["material_id"])
        entry_dict = entry.as_dict()
        compound['mp_entry'] = entry_dict
        try:
            cif_txt = entry_dict['data']['cif']
            name = '{}_{}.cif'.format(compound['material_id'], c)
            return_list.append({'content': cif_txt, 'name': name, 'type':'ciffile'})
        except Exception as e:
            if c in struc_set:
                continue
            
            struc_set.add(c)    
            struc_list.append([m.get_structure_by_material_id(compound['material_id']), c])
            
    if return_list:
        return return_list
    else:
        return struc_list
